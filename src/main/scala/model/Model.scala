package model

object Model {
  sealed abstract class Offer
  final case class DirectDiscount(quantity: Int, discount: Double) extends Offer
  final case class DependencyDiscount(productName: String, quantity: Int, discount: Double) extends Offer
  final case object NoOffer extends Offer

  final case class ShoppingItem(productName: String, count: Int, price: Price)

  final case class Price(amount: Double, currency: String)
  final case class DiscountApplied(productName: String, offer: Double, discount: Double)
  final case class PrintableResult(subTotal: Double, total: Double, discounts: List[DiscountApplied])

}
