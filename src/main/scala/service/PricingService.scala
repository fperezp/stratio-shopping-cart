package service

import com.typesafe.config.Config
import model.Model.{DependencyDiscount, DirectDiscount, Offer, Price}

import scala.collection.JavaConverters._
import scala.language.postfixOps

abstract class PricingService {

  def getPrice(productName: String): Price

  def getOffers(): Map[String, Offer]
}

class MockPricingService(implicit config: Config) extends PricingService {

  val Prices: Map[String, Price] = config.getConfigList("prices").asScala map { onePrice =>
    onePrice.getString("product") ->
      Price(
        amount = onePrice.getDouble("price"),
        currency = onePrice.getString("currency"))
  } toMap

  def getPrice(productName: String): Price =
    Prices.getOrElse(productName, Price(0d, "EUR"))

  override def getOffers(): Map[String, Offer] = config.getConfigList("offers").asScala map { oneOffer =>

    oneOffer.getString("type") match {
      case "direct" =>
        oneOffer.getString("product") ->
          DirectDiscount(
            quantity = oneOffer.getInt("amount"),
            discount = oneOffer.getDouble("discount"))
      case "dependency" =>
        oneOffer.getString("product") ->
          DependencyDiscount(
            productName = oneOffer.getString("dependency"),
            quantity = oneOffer.getInt("amount"),
            discount = oneOffer.getDouble("discount"))
    }

  } toMap
}
