package service

import com.typesafe.config.Config
import model.Model._
import implicits.Toolkit._

class ShoppingCartService(implicit pricingService: PricingService, config: Config) {

  private val offers = pricingService.getOffers()

  def getShoppingItems(items: List[String]): List[ShoppingItem] = items.foldRight(List.empty[ShoppingItem]) { (item, acc) =>

    val currentItemAcc = acc.find(_.productName == item)
    val currentQuantity: Int = currentItemAcc.map(_.count).sum + 1
    val currentItemPrice: Price = pricingService.getPrice(item)

    acc.filterNot(_.productName == item) :+ ShoppingItem(item, currentQuantity, currentItemPrice)
  }

  def billApplyingDiscounts(items: List[ShoppingItem])(implicit exchangeService: ExchangeService): PrintableResult =
    items.foldLeft(PrintableResult(0, 0, Nil)) { (result, oneItem) =>

      val productOffers = offers.getOrElse(
        oneItem.productName,
        NoOffer
      )

      productOffers match {
        case DirectDiscount(quantity, discount) if oneItem.count >= quantity =>
          val totalPrice = oneItem.price.toCurrency().amount * oneItem.count
          val priceWithDiscount = (totalPrice * (1 - discount)).round(2)

          result.copy(
            subTotal = (result.subTotal + totalPrice).round(2),
            total = (result.total + priceWithDiscount).round(2),
            discounts = result.discounts :+ DiscountApplied(oneItem.productName, discount, (totalPrice - priceWithDiscount).round(2))
          )

        case DependencyDiscount(productName, quantity, discount) if checkConditions(productName, items, quantity) =>
          val totalPrice = oneItem.price.toCurrency().amount * oneItem.count
          val priceWithDiscount = totalPrice * (1 - discount)

          result.copy(
            subTotal = (result.subTotal + totalPrice).round(2),
            total = (result.total + priceWithDiscount).round(2),
            discounts = result.discounts :+ DiscountApplied(oneItem.productName, discount, totalPrice - priceWithDiscount)
          )

        case _ =>
          val totalPrice = oneItem.price.toCurrency().amount * oneItem.count

          result.copy(
            subTotal = (result.subTotal + totalPrice).round(2),
            total = (result.total + totalPrice).round(2)
          )
      }
    }

  def checkConditions(productName: String, items: List[ShoppingItem], quantity: Int): Boolean =
    items.find(_.productName == productName).fold(false)(_.count >= quantity)

  def printShoppingCart(shoppingCart: PrintableResult): Unit = {

    print(s"subTotal\t\t: ${shoppingCart.subTotal}€")
    if (shoppingCart.discounts.isEmpty) print("(No offers available)\n")
    else print("\n")

    shoppingCart.discounts foreach { offer =>

      println(s"${offer.productName} ${offer.offer * 100}% off\t: -${offer.discount}€")

    }
    println(s"total\t\t\t: ${shoppingCart.total}€")

  }
}