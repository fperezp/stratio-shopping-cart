package service

abstract class ExchangeService {
  def getExchangeRate(from: String, to: String): Double
}

class ExchangeServiceMock() extends ExchangeService {
  def getExchangeRate(from: String, to: String): Double =
    (from, to) match {
      case ("GB", "EUR") => 1/0.9
      case _ => 1
    }
}

object ExchangeService {
  final case object ExchangeRateError
}

