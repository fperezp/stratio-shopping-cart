package implicits

import model.Model.Price
import service.ExchangeService

object Toolkit {

  implicit class DoubleToolkit(double: Double) {
    def round(precision: Int): Double =
      BigDecimal(double).setScale(precision, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  implicit class PriceToolkit(price: Price)(implicit exchangeService: ExchangeService) {
    def toCurrency(currency: String = "EUR"): Price =

      Price(
        (exchangeService.getExchangeRate(price.currency, currency) * price.amount).round(2),
        currency
      )
  }

}
