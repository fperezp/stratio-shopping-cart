import com.typesafe.config.ConfigFactory
import service.{ExchangeServiceMock, MockPricingService, ShoppingCartService}

object ShoppingCart extends App {

  implicit val config = ConfigFactory.load()
  implicit val exchangeService: ExchangeServiceMock = new ExchangeServiceMock()
  implicit val pricingService: MockPricingService = new MockPricingService()

  val shoppingCartService = new ShoppingCartService()
  val shoppingItems = shoppingCartService.getShoppingItems(args.toList)
  val shoppingCart = shoppingCartService.billApplyingDiscounts(shoppingItems)

  shoppingCartService.printShoppingCart(shoppingCart)
}
