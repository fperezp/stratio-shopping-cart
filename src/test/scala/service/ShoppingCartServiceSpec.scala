package service

import com.typesafe.config.ConfigFactory
import model.Model.{DirectDiscount, DiscountApplied, Offer, Price, PrintableResult, ShoppingItem}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpecLike}
import org.mockito.Mockito._
import org.mockito.Matchers._

class ShoppingCartServiceSpec extends WordSpecLike with Matchers with MockitoSugar {

  implicit val pricingService = mock[PricingService]
  implicit val config = ConfigFactory.load()
  implicit val exchangeSerive = mock[ExchangeService]

  when(pricingService.getOffers()).thenReturn(Map("Apples" -> DirectDiscount(2, 0.50)))
  when(pricingService.getPrice(any())).thenReturn(Price(1.0, "EUR"))
  when(exchangeSerive.getExchangeRate(any(), any())).thenReturn(1)

  // we can unit test with any possible configuration
  //implicit val config = ConfigFactory.parseString(
  //  """
  //    |
  //  """.stripMargin)

  val shoppingCartService = new ShoppingCartService()

  "ShoppingCartService" should {

    "round should round up when next precision digit is greater than 5" in {
      val result = List(
        ShoppingItem("Bread", 1, Price(1.0, "EUR")),
        ShoppingItem("Soup", 1, Price(1.0, "EUR")),
        ShoppingItem("Banana", 2, Price(1.0, "EUR")))

      shoppingCartService.getShoppingItems(
        List("Banana", "Soup", "Bread", "Banana")) shouldBe result
    }

    "" in {

      val items = List(
        ShoppingItem("Bread", 1, Price(1.0, "EUR")),
        ShoppingItem("Soup", 1, Price(1.0, "EUR")),
        ShoppingItem("Banana", 2, Price(1.0, "EUR")))

      val result = PrintableResult(4.00, 4.00, List())

      shoppingCartService.billApplyingDiscounts(items) shouldBe result
    }

    "1" in {
      when(pricingService.getOffers()).thenReturn(Map("Apples" -> DirectDiscount(2, 0.50)))

      val items = List(
        ShoppingItem("Bread", 1, Price(1.0, "EUR")),
        ShoppingItem("Soup", 1, Price(1.0, "EUR")),
        ShoppingItem("Apples", 2, Price(1.0, "EUR")))

      val result = PrintableResult(4.0, 3.0, List(DiscountApplied("Apples", 0.50, 1.00)))

      shoppingCartService.billApplyingDiscounts(items) shouldBe result
    }
  }

}
