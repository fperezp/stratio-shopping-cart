package implicits

import model.Model.Price
import org.scalatest.{Matchers, WordSpecLike}
import service.ExchangeServiceMock

class ToolkitSpec extends WordSpecLike with Matchers {

  "DoubleToolkit" should {

    import implicits.Toolkit.DoubleToolkit

    "round should round up when next precision digit is greater than 5" in (0.149.round(2) shouldBe 0.15)
    "round should round down when next precision digit is lower than 5" in (0.144.round(2) shouldBe 0.14)
  }

  "PriceToolkit" should {

    implicit val exchangeService: ExchangeServiceMock = new ExchangeServiceMock()
    import implicits.Toolkit.PriceToolkit

    "toCurrency should round to EUR by default" in (Price(0.9, "GB").toCurrency() shouldBe Price(1, "EUR"))
  }
}
