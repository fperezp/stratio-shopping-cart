# Stratio - Coding exercise
## Shopping Cart

### Build the project
sbt assembly

### Run the project

java -jar target/ShoppingCart.jar Apples Peanuts Soup Peanuts Bread

### Run Tests
sbt test

### Outputs
java -jar target/ShoppingCart.jar Apples Peanuts Soup Peanuts Bread

```
subTotal		: 6.72€(No offers available)
total			: 6.72€ 
```

java -jar target/ShoppingCart.jar Apples Peanuts Soup Apples Peanuts Bread Soup

```
subTotal		: 8.55€
Bread 50.0% off	: -0.445€
Apples 10.0% off	: -0.22€
total			: 7.89€
```

java -jar target/ShoppingCart.jar Apples Peanuts Soup Apples Peanuts Bread

```
subTotal		: 7.83€
Apples 10.0% off	: -0.22€
total			: 7.61€
```
