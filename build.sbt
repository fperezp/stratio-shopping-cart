name := "shopping-cart"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= List(
  "com.typesafe"  % "config"        % "1.3.4",
  "org.scalatest" %% "scalatest"    % "3.0.5" % Test,
  "org.mockito"   % "mockito-all"   % "1.10.19" % Test)

assemblyOutputPath in assembly := file("target/ShoppingCart.jar")
